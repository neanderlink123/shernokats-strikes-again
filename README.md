# Introdução
Um cachorro |modificado genéticamente| foi abduzido por felinos do espaço sideral. Ele ganhou um dispositivo que controla a própria gravidade, além de possuir armas laser anti-shernokats.
Ele deve coletar o máximo de DNAs de Shernokats para que possa fugir da base da Meia-Lua.

# Mecânica
Infinite Runner. O personagem irá correr infinitamente, sendo necessário desviar de obstáculos e matar o máximo de inimigos possíveis. Ocasionalmente um Chefão irá surgir.
Quanto mais inimigos matar, mais DNAs o jogador ganhará.
O jogador terá vidas e energias.

    Vidas:
    Sempre que o jogador for atingido por um inimigo, ele perderá uma vida e entrará ficará invulnerável por alguns segundos.
    Quando a vida chegar a zero, o jogador perde.
    Ainda a fazer, mas será possível aumentar a capacidade máxima de vida. Também pode-se discutir a ideia de recuperação de vida durante a partida.
    Energias:
    O jogador não pode ficar trocando a gravidade infinitamente sem tocar o chão. Sempre que o jogador tocar o chão, suas energias serão reestabelecidas.
    Ainda a fazer, mas será possível aumentar a capacidade máxima de energias.

O jogador poderá atirar para matar os inimigos. Os inimigos deixarão DNAs ao morrer, que serão computados ao total do jogador.

Os DNAs recuperados pelo jogador serão armazenados e poderão ser usados em uma loja. Esta loja dará a possibilidade de customização do jogador.
Skins, aumento da força gravitacional, armas mais fortes, mais capacidade de vida e de energias estarão disponíveis.

Conquistas são objetivos a serem completados durante o gameplay. Ao completar uma conquista, uma quantia de DNAs será recebida.
Exemplos: 
    1. Matar 10 Shernokats sem trocar a gravidade.
    2. Destruir o Boss sem receber dano.
    3. Sobreviver por 10 segundos sem receber dano ou matar um Shernokat.
    4. Matar 25 Shernokats.


Ainda existem muitas coisas a fazer, tentarei listar algumas aqui. Se tiverem alguma ideia, coloquem-a nesse documento, assim todos têm acesso.
    


# Estética
Tema espacial mas meio bizarro, com animais modificados. Ainda sem muita definição. Sintam-se livres para adicionarem o que acharem interessante.

# Baixar o projeto
Para baixar o projeto, você deve ir na Aba **CODE**. Depois, vá em **Clone** que fica no campo superior direito da tela. Aparecerá uma tela com uma url, use algum gerenciador de repositórios,
como o SourceTree ou TortoiseGit e faça o clone. Também é possível fazê-lo pela linha de comando, basta utilizar o link que é passado por lá. 

# Builds
As builds se encontram na pasta Builds na raiz do projeto. Tentarei deixar uma build atualizada sempre que tiver mudanças significantes para testarmos. Ficar de olho no número da versão, 
pois ela dirá qual é a mais recente. Por praticidade, pode-se apenas baixar o projeto como um arquivo ZIP e acessar a pasta Builds para testar uma versão mais atual.

# IMPORTANTE
Sempre que for fazer um Push no projeto, avisar com antecedência para que não aja conflitos.