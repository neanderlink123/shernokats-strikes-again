﻿using UnityEngine;
using UnityEngine.UI;

public class ConfiguracoesController : MonoBehaviour
{
    public Slider slider;
    public Dropdown dropdown;

    public GameObject config;

    public void Start()
    {
        Debug.Log(QualitySettings.GetQualityLevel());
    }

    private void PegarPreferencias ()
    {
        slider.value = PlayerPrefs.GetFloat("Volume", 1);
        dropdown.value = QualitySettings.GetQualityLevel();
    }

    public void FecharConfiguracoes ()
    {
        config.SetActive(false);
    }

    public void SalvarConfiguracoes ()
    {
        PlayerPrefs.SetFloat("Volume", slider.value);
        PlayerPrefs.SetInt("Qualidade", dropdown.value);
        QualitySettings.SetQualityLevel(dropdown.value);
        FecharConfiguracoes();
    }

    public void MostrarConfiguracoes ()
    {
        config.SetActive(true);
        PegarPreferencias();
    }

}
    