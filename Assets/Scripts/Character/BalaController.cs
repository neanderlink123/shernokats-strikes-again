﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class BalaController : MonoBehaviour
{
    [SerializeField]
    private float velocidadeTiro = 8f;

    private Rigidbody2D rb;
    private Animator animator;
    private AudioSource audioColisao;

    public void Start()
    {
        audioColisao = GetComponent<AudioSource>();
        audioColisao.volume *= PlayerPrefs.GetFloat("Volume", 1);
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        rb.gravityScale = 0;
    }

    public void Update()
    {
        rb.velocity = new Vector2(velocidadeTiro, rb.velocity.y);

        foreach (var item in FindObjectsOfType<IdleArmoredShernokat>())
        {
            Physics2D.IgnoreCollision(transform.GetComponent<Collider2D>(), item.GetComponent<Collider2D>());
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Collider2D>().gameObject.CompareTag("Boss") || collision.GetComponent<Collider2D>().gameObject.CompareTag("Shernokat"))
        {
            if (audioColisao != null)
                audioColisao.Play(); 

            velocidadeTiro = 0;
            animator.SetTrigger("TerminouTiro");
        }
    }

    public void OnBecameInvisible()
    {
        DestruirTiro();
    }

    public void DestruirTiro()
    {
        Destroy(gameObject);

    }

    public void OnValidate()
    {
        if (velocidadeTiro < 0)
        {
            velocidadeTiro = 0;
        }
    }
}
