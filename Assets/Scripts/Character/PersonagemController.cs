﻿using Assets.Scripts.Repositories.Repos;
using Assets.Scripts.Repositories.ScriptableObjects;
using System.Collections;
using System.Globalization;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody2D))]
public class PersonagemController : MonoBehaviour, IAtributo, IMortal, IMovimento
{
    [Header("Config. Mecânica")]
    [SerializeField]
    private float _velMove = 5f;
    [SerializeField]
    private float _forcaPulo = 8f, _forcaGravitacional = 2f, _tempoImortal = 1.5f;

    [Header("Config. Jogabilidade")]

    [SerializeField]
    private int _vidaMax;
    [SerializeField]
    private int _vidaAtual, _qtdePulos, _maxPulos = 3;

    private GameObject prefabVida;

    [SerializeField]
    private Rigidbody2D _rb;
    private AudioSource audioPassos, audioMorrendo, audioGravidade;
    private Color _corInicial;
    private ArmaController _arma;
    private InterfaceController _uiController;


    private bool _isMorto, _isImortal, _isVelAumentada, _isPiscando, _isProtegido;
    private PlayerRepository _playerRepository;

    public PlayerScriptable prefs;
    private Action action;

    public float VelocidadeMove
    {
        get { return _velMove; }
        set { _velMove = value; }
    }
    public float ForcaPulo
    {
        get { return _forcaPulo; }
        set { _forcaPulo = value; }
    }
    public int VidaMax
    {
        get { return _vidaMax; }
        set { _vidaMax = value; }
    }
    public int VidaAtual
    {
        get { return _vidaAtual; }
        set { _vidaAtual = value; }
    }
    public bool IsMorto
    {
        get { return _isMorto; }
        set { _isMorto = value; }
    }

    public int QtdePulos
    {
        get { return _qtdePulos; }
        set { _qtdePulos = value; }
    }

    public bool IsProtegido
    {
        get { return _isProtegido; }
        set { _isProtegido = value; }
    }

    public ArmaController Arma
    {
        get { return _arma; }
    }

    void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        _corInicial = GetComponent<SpriteRenderer>().color;
        _arma = GetComponent<ArmaController>();
        _uiController = Camera.main.GetComponent<InterfaceController>();
        _playerRepository = new PlayerRepository();

        audioPassos = GetComponents<AudioSource>().Single(x => x.clip.name.Equals("Passos"));
        audioMorrendo = GetComponents<AudioSource>().Single(x => x.clip.name.Equals("PlayerMorrendo"));
        audioGravidade = GetComponents<AudioSource>().Single(x => x.clip.name.Equals("TrocaGravidade"));
        prefs = _playerRepository.GetSingle(x => x);
        SetarPreferenciasVazias();

        PegarValoresPlayerPrefs();

        InicializarCampos();
        prefabVida = Resources.Load<GameObject>("Prefabs/Colecionavel/Colecionavel_Vida");
    }

    void Start()
    {
        _uiController.SetPainelVidas();
        _uiController.SetPainelEnergias();
    }

    void FixedUpdate()
    {
        if (_isMorto)
        {
            Morrer();
        }
        else
        {
            Controlar();
        }
    }

    void LateUpdate()
    {
        _isMorto = VerificarMorte();

        if (_isImortal)
        {
            if (!_isPiscando && !_isMorto)
            {
                StartCoroutine(PiscarPersonagem());
            }
        }
        else
        {
            //GetComponent<SpriteRenderer>().color = _corInicial;
            if (!_isVelAumentada)
            {
                StartCoroutine(AumentarVelocidade());
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Plataforma"))
        {
            QtdePulos = _maxPulos;
            _uiController.SetPainelEnergias();
            GetComponent<Animator>().SetBool("IsGrounded", true);
        }
    }


    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Plataforma"))
        {
            GetComponent<Animator>().SetBool("IsGrounded", true);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Plataforma"))
        {
            GetComponent<Animator>().SetBool("IsGrounded", false);
        }
    }

    public void Morrer()
    {
        if (!GetComponent<Animator>().GetBool("Died"))
        {
            Camera.main.GetComponent<AudioSource>().Stop();
            audioMorrendo.Play();
        }

        _velMove = 0;
        var fim = GameObject.Find("Canvas").transform.Find("Painel_GameOver");
        fim.Find("TxtPontos").GetComponent<Text>().text = string.Format("Você coletou {0:0} DNAs.", GetComponent<PontuacaoController>().pontuacao);
        fim.gameObject.SetActive(true);

        GetComponent<Animator>().SetBool("Died", true);
    }

    public void DiminuirColisao()
    {
        GetComponent<CircleCollider2D>().radius = 0.07f;
    }

    public void Mover()
    {
        _rb.velocity = new Vector2(VelocidadeMove, _rb.velocity.y);
    }

    public void Pular()
    {
        _rb.gravityScale *= -1;
        audioGravidade.Play();
        GetComponent<Animator>().SetBool("IsGrounded", false);
        transform.localScale = new Vector3(transform.localScale.x, -transform.localScale.y);
    }

    public bool VerificarMorte()
    {
        if (VidaAtual <= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void Controlar()
    {
        Mover();

        if (Input.GetButtonDown("Jump") && QtdePulos > 0)
        {
            Pular();
            QtdePulos--;
            _uiController.SetPainelEnergias();
        }

        if (Input.GetButtonDown("Fire1"))
        {
            Arma.Atirar();
        }

        if (Input.GetKeyDown(KeyCode.F) && action)
        {
            UsarSkill();
        }
    }

    public void RecebeuDano(int qtdeVidaPerdida)
    {
        if (!_isImortal)
        {
            if (!_isProtegido)
            {
                VidaAtual -= qtdeVidaPerdida;
                _uiController.SetPainelVidas();
            }
            else
            {
                _isProtegido = false;
                transform.Find("Escudo").gameObject.SetActive(false);
            }
            StartCoroutine(RespawnImortal());
        }
    }

    private void LancarMorte()
    {
        GetComponent<Collider2D>().isTrigger = true;
        _rb.AddForce(new Vector3(_forcaGravitacional * 0.3f, _forcaGravitacional * 0.5f), ForceMode2D.Impulse);
    }

    public void ExecutarSomPassos()
    {
        audioPassos.Play();
    }

    public void AumentarVida(int qtde, Vector3 posicao)
    {
        _vidaAtual += qtde;
        ColecionavelController.MostrarColecionavel(qtde, prefabVida, posicao);
        Camera.main.GetComponent<InterfaceController>().SetPainelVidas();
    }

    private IEnumerator RespawnImortal()
    {
        _isImortal = true;
        yield return new WaitForSeconds(_tempoImortal);
        _isImortal = false;
    }

    private IEnumerator AumentarVelocidade()
    {
        _isVelAumentada = true;
        if (!GameObject.Find("PoolPlataformas").GetComponent<GeradorCenario>().isBoss)
        {
            VelocidadeMove += 0.3f;
        }
        GetComponent<Animator>().SetFloat("MultCorrida", GetComponent<Animator>().GetFloat("MultCorrida") + 0.015f);
        yield return new WaitForSeconds(3f);
        _isVelAumentada = false;
    }


    private IEnumerator PiscarPersonagem()
    {
        _isPiscando = true;
        var transparent = new Color(0, 0, 0, 0);
        GetComponent<SpriteRenderer>().enabled = false;
        yield return new WaitForSeconds(0.1f);
        GetComponent<SpriteRenderer>().enabled = true;
        yield return new WaitForSeconds(0.2f);
        _isPiscando = false;
    }

    private void SetarPreferenciasVazias()
    {
        if (prefs.arma == null)
        {
            var novaArma = ScriptableObject.CreateInstance<ItemScriptable>();
            novaArma.tipoItem = TipoItem.Arma;
            novaArma.valores = new string[] { "1.5" };
            prefs.arma = novaArma;
        }

        if (prefs.mochila == null)
        {
            var novaMochila = ScriptableObject.CreateInstance<ItemScriptable>();
            novaMochila.tipoItem = TipoItem.Mochila;
            novaMochila.valores = new string[] { _vidaMax.ToString(), _maxPulos.ToString() };
            prefs.mochila = novaMochila;
        }

        if (prefs.roupa == null)
        {
            var novaRoupa = ScriptableObject.CreateInstance<ItemScriptable>();
            novaRoupa.tipoItem = TipoItem.Roupa;
            novaRoupa.valores = new string[] { "#8E2CA4", _forcaGravitacional.ToString() };
            prefs.roupa = novaRoupa;
        }
    }

    private void InicializarCampos()
    {
        _rb.gravityScale = _forcaGravitacional;
        QtdePulos = _maxPulos;
        _vidaAtual = _vidaMax;
    }

    private void UsarSkill()
    {
        action.Executar();
        action = null;
        prefs.skill = null;
        InterfaceController.SetarTextoHabilidade("Sem habilidade");

        _playerRepository.Salvar(new PlayerScriptable[] { prefs });
    }

    private void PegarValoresPlayerPrefs()
    {
        SetarPrefs(prefs.arma);
        SetarPrefs(prefs.roupa);
        SetarPrefs(prefs.mochila);        

        action = Resources
            .LoadAll<Action>("Objects/Skills/Actions")
            .FirstOrDefault(x => x.GetType().Name == prefs.skill.classeAction);


        InterfaceController.SetarTextoHabilidade(action?.nomeSkill ?? "Sem habilidade");
    }

    private void SetarPrefs(ItemScriptable itemEquipado)
    {
        switch (itemEquipado?.tipoItem)
        {
            case TipoItem.Roupa:
                var cor = new Color();
                ColorUtility.TryParseHtmlString(itemEquipado?.valores[0], out cor);
                GetComponentsInChildren<SpriteRenderer>().First(x => x.name.Equals("Roupa")).color = cor;
                _forcaGravitacional = float.Parse(itemEquipado?.valores[1], CultureInfo.InvariantCulture);
                break;
            case TipoItem.Arma:
                _arma.tempoRespawn = float.Parse(itemEquipado?.valores[0], CultureInfo.InvariantCulture);
                break;
            case TipoItem.Mochila:
                _vidaMax = int.Parse(itemEquipado?.valores[0], CultureInfo.InvariantCulture);
                _maxPulos = int.Parse(itemEquipado?.valores[1], CultureInfo.InvariantCulture);
                break;
            default:
                break;
        }
    }

}
