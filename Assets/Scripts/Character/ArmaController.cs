﻿using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ArmaController : MonoBehaviour
{
    public GameObject tiro;
    private Vector2 _posicaoTiro;

    public float tempoRespawn;
    private bool _emRespawn = false;

    public Slider slider;
    public Color corRespawn;
    private Color corAtiravel;
    private AudioSource audioTiro;

    public bool EmRespawn
    {
        get
        {
            return _emRespawn;
        }

        set
        {
            _emRespawn = value;
        }
    }

    private void Start()
    {
        audioTiro = GetComponentsInChildren<AudioSource>().Single(x => x.name == "PosiTiro");
        corAtiravel = slider.transform.Find("Fill Area").Find("Fill").GetComponent<Image>().color;
    }

    public void Atirar()
    {
        _posicaoTiro = GetPosicaoTiro("PosiTiro").transform.position;
        if (!EmRespawn)
        {
            Instantiate(tiro, _posicaoTiro, Quaternion.identity);

            if (audioTiro != null)
                audioTiro.Play();

            StartCoroutine(WaitRespawnTiro(tempoRespawn));
        }
    }

    public GameObject GetPosicaoTiro (string nomeObj)
    {
        return transform.Find(nomeObj).gameObject;
    }

    public IEnumerator WaitRespawnTiro (float tempoEspera)
    {
        EmRespawn = true;
        slider.transform.Find("Fill Area").Find("Fill").GetComponent<Image>().color = corRespawn;
        for (float i = 0; i <= tempoEspera; i+= 0.01f)
        {
            slider.value = i / tempoEspera;
            slider.transform.Find("Fill Area").Find("Fill").GetComponent<Image>().color = Color.Lerp(corRespawn, corAtiravel, slider.value);
            yield return new WaitForSeconds(0.01f);
        }
        EmRespawn = false;
    }
}
