﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class Inicializador : MonoBehaviour
{
    public Text textoInicio;
    public bool inicio;

    public void Start()
    {
        textoInicio.text = "Aperte algum botão para iniciar";
    }

    public void Update()
    {
        if (Input.anyKeyDown && !Input.GetKeyDown(KeyCode.Escape) && !inicio)
        {
            inicio = true;
            textoInicio.gameObject.SetActive(false);
            GameObject.FindWithTag("Player").GetComponent<PersonagemController>().enabled = true;
            GameObject.FindWithTag("Player").GetComponent<Animator>().SetTrigger("Iniciou");
            GameObject.Find("PoolPlataformas").GetComponent<GeradorCenario>().enabled = true;
            Camera.main.transform.Find("Spamador").GetComponent<GeradorShernokats>().enabled = true;
            Camera.main.GetComponent<AudioSource>().Play();
            Camera.main.GetComponent<ConquistaController>().enabled = true;
            Destroy(this);
        }
    }
}
