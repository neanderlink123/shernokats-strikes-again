﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PontuacaoController : MonoBehaviour {

    public float pontuacao;
    PersonagemController p;

    public bool entregouDnas = false;
    public GameObject prefabDna;

    // Use this for initialization
    void Start () {
        p = GetComponent<PersonagemController>();
        prefabDna = Resources.Load<GameObject>("Prefabs/Colecionavel/Colecionavel_DNA");
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (!p.IsMorto)
        {
            if (pontuacao % 50 == 0 && pontuacao > 0)
            {
                MessageController.MostrarMensagem("UAU! Você já coletou " + pontuacao + " DNAs!");
            }
        }else
        {
            if (pontuacao > PlayerPrefs.GetFloat("PontuacaoMaxima"))
            {
                PlayerPrefs.SetFloat("PontuacaoMaxima", pontuacao);
            }

            if (!entregouDnas)
            {
                entregouDnas = true;
                PlayerPrefs.SetFloat("TotalDna", PlayerPrefs.GetFloat("TotalDna") + pontuacao);
            }
        }
    }

    public void AdicionarPontos (int qtde, Vector3 posicao)
    {
        pontuacao += qtde;
        ColecionavelController.MostrarColecionavel(qtde, prefabDna, posicao);
    }
}
