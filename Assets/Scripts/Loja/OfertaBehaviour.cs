﻿using Assets.Scripts.Repositories.Repos;
using Assets.Scripts.Repositories.ScriptableObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class OfertaBehaviour : MonoBehaviour
{
    public ItemScriptable oferta;

    public void Start()
    {
        InicializarOferta();
        GetComponent<Button>().onClick.AddListener(MostrarOferta);
    }

    private void InicializarOferta()
    {
        var isItemComprado = false;
        var inventario = Camera.main.GetComponent<InventarioController>().inventario;
        
        inventario.ForEach(x =>
        {
            if (x.isComprado && x.nome.Equals(oferta.nome) && x.tipoItem != TipoItem.Consumivel)
            {
                isItemComprado = true;
            }
        });

        StartCoroutine(ItemBehaviour.CarregarImagem(GetComponentsInChildren<Image>().First(x => x.name.Equals("ImgIcone")), oferta.pathSpriteIcone));

        if (isItemComprado)
        {
            GetComponent<Image>().color = Color.gray;
            GetComponentsInChildren<Image>().First(x => x.name.Equals("ImgIconeDNA")).color = new Color(0, 0, 0, 0);
        }

        transform.Find("TxtTitulo").GetComponent<Text>().text = oferta.nome;
        transform.Find("TxtDescricao").GetComponent<Text>().text = oferta.descricao;
        transform.Find("TxtValor").GetComponent<Text>().text = isItemComprado ? "Item comprado" : string.Format("x{0}", oferta.valorCompra);
    }

    public void ApresentarTelaOferta(ItemScriptable item, GameObject goOferta)
    {
        Transform painelOferta = goOferta.transform.Find("PainelOferta");

        painelOferta.GetComponentsInChildren<Text>().First(x => x.name.Equals("TxtTitulo")).text = item.nome;
        painelOferta.GetComponentsInChildren<Text>().First(x => x.name.Equals("TxtDescricao")).text = item.descricao;
        painelOferta.GetComponentsInChildren<Text>().First(x => x.name.Equals("TxtValor")).text = string.Format("x{0}", item.valorCompra);
        StartCoroutine(ItemBehaviour.CarregarImagem(painelOferta.GetComponentsInChildren<Image>().First(x => x.name.Equals("ImgIcon")), item.pathSpriteIcone));

        var isItemComprado = false;
        var inventario = Camera.main.GetComponent<InventarioController>().inventario;
        inventario.ForEach(x =>
        {
            if (x.isComprado && x.nome.Equals(item.nome) && x.tipoItem != TipoItem.Consumivel)
            {
                isItemComprado = true;
            }
        });

        SetarBotao(painelOferta, isItemComprado);
        goOferta.SetActive(true);
    }

    private void SetarBotao(Transform painelOferta, bool isItemComprado)
    {
        if (isItemComprado)
        {
            painelOferta.GetComponentsInChildren<Button>().FirstOrDefault(x => x.name.Equals("BtnComprar")).interactable = false;
            painelOferta.GetComponentsInChildren<Button>().FirstOrDefault(x => x.name.Equals("BtnComprar")).GetComponentInChildren<Text>().text = "Já possui";
            painelOferta.GetComponentsInChildren<Button>().FirstOrDefault(x => x.name.Equals("BtnComprar")).onClick.RemoveAllListeners();
        }
        else
        {
            painelOferta.GetComponentsInChildren<Button>().FirstOrDefault(x => x.name.Equals("BtnComprar")).interactable = true;
            painelOferta.GetComponentsInChildren<Button>().FirstOrDefault(x => x.name.Equals("BtnComprar")).GetComponentInChildren<Text>().text = "COMPRAR";
            painelOferta.GetComponentsInChildren<Button>().FirstOrDefault(x => x.name.Equals("BtnComprar")).onClick.RemoveAllListeners();
            painelOferta.GetComponentsInChildren<Button>().FirstOrDefault(x => x.name.Equals("BtnComprar")).onClick.AddListener(ComprarItem);
        }
    }

    public void MostrarOferta()
    {
        ApresentarTelaOferta(oferta, GameObject.Find("Canvas").transform.Find("Oferta").gameObject);
    }

    public void ComprarItem ()
    {
        if (PlayerPrefs.GetFloat("TotalDna") >= oferta.valorCompra)
        {
            oferta.isComprado = true;
            PlayerPrefs.SetFloat("TotalDna", PlayerPrefs.GetFloat("TotalDna") - oferta.valorCompra);
            Camera.main.GetComponent<InventarioController>().AdicionarItemNoInventario(oferta);
            Camera.main.GetComponent<InventarioController>().SalvarInventario();
            Camera.main.GetComponent<LojaController>().EsconderOferta(GameObject.Find("Canvas").transform.Find("Oferta").gameObject);
            Camera.main.GetComponent<EventosMenu>().MostrarErro("Item comprado com sucesso!");
            EventosMenu.AtualizarDnas();
        }
        else
        {
            Camera.main.GetComponent<EventosMenu>().MostrarErro("Você não tem DNAs suficiente para a compra");
        }
    }

}

