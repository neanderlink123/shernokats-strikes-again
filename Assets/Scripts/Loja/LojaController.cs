﻿using Assets.Scripts.Repositories.ScriptableObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class LojaController : MonoBehaviour
{
    public List<ItemScriptable> ofertas;
    public List<ItemScriptable> ofertasFiltradas;

    void Start()
    {
        ofertas = Resources
            .LoadAll<ItemScriptable>("Objects/Itens")
            .ToList();

        var skills = Resources
            .LoadAll<SkillScriptable>("Objects/Skills")
            .Select(x => (ItemScriptable)x)
            .ToList();

        ofertas.AddRange(skills);

        MostrarTudo(GameObject.Find("Canvas")
            .transform
            .Find("Interface")
            .Find("BotoesFiltro")
            .GetChild(0)
            .GetComponent<Button>());
    }

    public void Filtrar(Func<ItemScriptable, bool> filtro)
    {
        ofertasFiltradas = ofertas.Where(filtro).ToList();
        MostrarOfertas<ItemScriptable>();
    }

    public void MostrarOfertas<TProperty>(Func<ItemScriptable, TProperty> ordenacao = null, bool isContrario = false)
    {
        if (ordenacao != null)
        {
            ofertasFiltradas = isContrario ? ofertasFiltradas.OrderByDescending(ordenacao).ToList() : ofertasFiltradas.OrderBy(ordenacao).ToList();
        }
        else
        {
            ofertasFiltradas = ofertasFiltradas.OrderBy(x => x.nome).ToList();
        }

        var painelOfertas = GameObject
            .Find("Canvas")
            .transform.Find("Interface")
            .Find("ScrollItemsLoja")
            .Find("Viewport")
            .Find("Content");

        InterfaceController.LimparPainel(painelOfertas.gameObject);

        var prefab = Resources.Load<GameObject>("Prefabs/UIs/ItemLoja");

        foreach (var oferta in ofertasFiltradas)
        {
            prefab.GetComponent<OfertaBehaviour>().oferta = oferta;
            Instantiate(prefab, painelOfertas);
        }
    }

    public void EsconderOferta(GameObject oferta)
    {
        oferta.SetActive(false);
    }

    private void SetarAbaSelecionada(Button botaoSelecionado)
    {
        var botoes = GameObject.Find("Canvas")
            .transform
            .Find("Interface")
            .Find("BotoesFiltro")
            .GetComponentsInChildren<Button>();

        foreach (var botao in botoes)
        {
            botao.gameObject.GetComponent<Image>().color = Color.white;
            botao.GetComponentInChildren<Text>().color = Color.black;
        }

        botaoSelecionado.image.color = new Color(0.29f, 0.44f, 0.52f);
        botaoSelecionado.GetComponentInChildren<Text>().color = Color.white;
    }

    //Eventos
    private void OrdenarLojaPorNome()
    {
        MostrarOfertas(x => x.nome);
    }

    private void OrdenarLojaPorMenoresPrecos()
    {
        MostrarOfertas(x => x.valorCompra);
    }

    private void OrdenarLojaPorMaioresPrecos()
    {
        MostrarOfertas(x => x.valorCompra, true);
    }

    public void FiltrarPorArmas(Button abaSelecionada)
    {
        Filtrar(x => x.tipoItem == TipoItem.Arma);

        SetarAbaSelecionada(abaSelecionada);
        TrocouOpcao(GameObject.Find("Canvas")
            .transform.Find("Interface")
            .Find("DropdownFiltro").GetComponent<Dropdown>());
    }

    public void FiltrarPorRoupas(Button abaSelecionada)
    {
        Filtrar(x => x.tipoItem == TipoItem.Roupa);

        SetarAbaSelecionada(abaSelecionada);
        TrocouOpcao(GameObject.Find("Canvas")
            .transform.Find("Interface")
            .Find("DropdownFiltro").GetComponent<Dropdown>());
    }

    public void FiltrarPorMochilas(Button abaSelecionada)
    {
        Filtrar(x => x.tipoItem == TipoItem.Mochila);

        SetarAbaSelecionada(abaSelecionada);
        TrocouOpcao(GameObject.Find("Canvas")
            .transform.Find("Interface")
            .Find("DropdownFiltro").GetComponent<Dropdown>());
    }

    public void MostrarTudo(Button abaSelecionada)
    {
        Filtrar(x => x.nome != null);

        SetarAbaSelecionada(abaSelecionada);
        TrocouOpcao(GameObject.Find("Canvas")
            .transform.Find("Interface")
            .Find("DropdownFiltro").GetComponent<Dropdown>());
    }

    public void TrocouOpcao(Dropdown dropdown)
    {
        switch (dropdown.value)
        {
            case 0:
                OrdenarLojaPorNome();
                break;
            case 1:
                OrdenarLojaPorMenoresPrecos();
                break;
            case 2:
                OrdenarLojaPorMaioresPrecos();
                break;
            default:
                throw new ArgumentException("Essa opção não foi tratada.");
        }
    }

}
