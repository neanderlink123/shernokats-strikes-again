﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMortal {
    bool IsMorto { get; set; }
    bool VerificarMorte();
    void Morrer();
}
