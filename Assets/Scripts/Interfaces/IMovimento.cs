﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public interface IMovimento
{
    float VelocidadeMove { get; set; }
    float ForcaPulo { get; set; }

    void Mover();
    void Pular();
}

