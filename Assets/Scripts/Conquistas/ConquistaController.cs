﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class ConquistaController : MonoBehaviour
{
    public List<ConquistaObject> conquistas;

    public void Start()
    {
        var conq = Conquista.CarregarConquistas();
        InicializarConquistas(conq);
    }

    private void InicializarConquistas(ConquistaWrapper conq)
    {
        foreach (var c in conq.conquistas)
        {
            ConquistaObject conquistaObject = c;
            conquistas.Add(conquistaObject);
            if (conquistaObject is ConquistaSobreviver && !conquistaObject.isConquistaCompleta)
            {
                StartCoroutine((conquistaObject as ConquistaSobreviver).VerificarSobrevivencia());
            }
        }
    }

    public void NotificarMissoes(TipoNotificacao tipo, int? valor)
    {
        switch (tipo)
        {
            case TipoNotificacao.MatouShernokatVoador:
                var m = conquistas.Where(x => x is ConquistaMatar && !x.isConquistaCompleta);
                if (m.Count() > 0)
                {
                    var conquista = m.ElementAt(0);

                    if (!conquista.isConquistaCompleta)
                    {
                        conquista.IsCompleto(valor);
                        Verificar(conquista);
                    }
                }
                break;
            case TipoNotificacao.MatouShernokatIdle:
                var m2 = conquistas.Where(x => x is ConquistaMatar && !x.isConquistaCompleta);
                if (m2.Count() > 0)
                {
                    var conquista = m2.ElementAt(0);

                    if (!conquista.isConquistaCompleta)
                    {
                        conquista.IsCompleto(valor);
                        Verificar(conquista);
                    }
                }
                break;
            case TipoNotificacao.MatouShernokatPulador:
                var m3 = conquistas.Where(x => x is ConquistaMatar && !x.isConquistaCompleta);
                if (m3.Count() > 0)
                {
                    var conquista = m3.ElementAt(0);

                    if (!conquista.isConquistaCompleta)
                    {
                        conquista.IsCompleto(valor);
                        Verificar(conquista);
                    }
                }
                break;
            default:
                break;
        }
    }

    public void Verificar(Conquista missao)
    {
        if (missao.isConquistaCompleta)
        {
            //conquistas.Remove(missao);
            NotificarJogadorMissaoCompleta(missao);
        }
    }

    public void NotificarJogadorMissaoCompleta(Conquista conquista)
    {
        MessageController.MostrarMensagem(string.Format("Você completou a conquista {0}!\n Você recebeu {1} DNAs de recompensa.", conquista.nome.ToUpper(), conquista.qtdeRecompensa));
        PlayerPrefs.SetFloat("TotalDna", PlayerPrefs.GetFloat("TotalDna") + conquista.qtdeRecompensa);
        Conquista.RegistrarAlteracoes();
    }

}
