﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum TipoNotificacao
{
    MatouShernokatVoador,
    MatouShernokatIdle,
    MatouShernokatPulador,
    Sobreviver,
    RecebeuDano

}
