﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ConquistaObject : ScriptableObject
{
    public string nome;
    public string descricao;
    public bool isConquistaCompleta;
    public int qtdeRecompensa;


    public virtual bool IsCompleto(int? valor)
    {
        return isConquistaCompleta;
    }

    public static implicit operator ConquistaObject(Conquista conquista)
    {
        if (conquista.qtdeMatar > 0) {
            var c = ScriptableObject.CreateInstance<ConquistaMatar>();

            c.nome = conquista.nome;
            c.qtdeRecompensa = conquista.qtdeRecompensa;
            c.descricao = conquista.descricao;
            c.isConquistaCompleta = conquista.isConquistaCompleta;
            c.qtdeMatar = conquista.qtdeMatar;
            return c;
        }else if (conquista.tempoSobrevivencia > 0)
        {
            var c = ScriptableObject.CreateInstance<ConquistaSobreviver>();

            c.nome = conquista.nome;
            c.qtdeRecompensa = conquista.qtdeRecompensa;
            c.descricao = conquista.descricao;
            c.isConquistaCompleta = conquista.isConquistaCompleta;
            c.tempoSobrevivencia = conquista.tempoSobrevivencia;
            return c;
        }
        else
        {
            var c = ScriptableObject.CreateInstance<ConquistaObject>();

            c.nome = conquista.nome;
            c.qtdeRecompensa = conquista.qtdeRecompensa;
            c.descricao = conquista.descricao;
            c.isConquistaCompleta = conquista.isConquistaCompleta;
            return c;
        }
    }

}
