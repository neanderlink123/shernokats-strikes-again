﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "Conquista_Sobrevivência", menuName = "Objetos/Conquista/Sobrevivência...")]
[Serializable]
public class ConquistaSobreviver : ConquistaObject
{
    [SerializeField]
    public float tempoSobrevivencia;
    
    public IEnumerator VerificarSobrevivencia ()
    {
        var isSobrevivido = true;

        var player = GameObject.FindWithTag("Player").GetComponent<PersonagemController>();

        for (float i = 0; i < tempoSobrevivencia; i+=0.01f)
        {
            var vidasPlayer = player.VidaAtual;
            if (vidasPlayer != player.VidaMax || player.Arma.EmRespawn || player.IsMorto)
            {
                isSobrevivido = false;
                break;
            }

            yield return new WaitForSeconds(0.01f);
        }

        if (isSobrevivido)
        {
            isConquistaCompleta = true;
            Camera.main.GetComponent<ConquistaController>().Verificar(this);
        }
    }
}
