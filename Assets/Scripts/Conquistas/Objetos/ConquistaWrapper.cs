﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class ConquistaWrapper : IWrapper
{
    [SerializeField]
    public Conquista[] conquistas;

    public int GetTotalObjetos ()
    {
        return conquistas.Count();
    }
}
