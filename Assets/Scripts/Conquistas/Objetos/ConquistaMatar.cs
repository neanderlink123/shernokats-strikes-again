﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "Conquista_Matar", menuName = "Objetos/Conquista/Matar...")]
[Serializable]
public class ConquistaMatar : ConquistaObject
{
    [SerializeField]
    public int qtdeMatar;

    public override bool IsCompleto(int? valor)
    {
        if (valor.HasValue)
        {
            qtdeMatar -= valor.Value;
        }

        if (qtdeMatar > 0)
        {
            return false;
        }
        else
        {
            isConquistaCompleta = true;
            return true;
        }
    }
}
