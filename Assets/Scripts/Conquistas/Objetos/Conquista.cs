﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class Conquista
{
    public static readonly string PATH = JsonHandler.PATH_SAVE_DATA + "conquistas.json";

    public string nome;
    public string descricao;
    public bool isConquistaCompleta;
    public int qtdeRecompensa;
    public int qtdeMatar;
    public float tempoSobrevivencia;

    public static implicit operator Conquista (ConquistaObject conquista)
    {
        var c = new Conquista
        {
            nome = conquista.nome,
            qtdeRecompensa = conquista.qtdeRecompensa,
            descricao = conquista.descricao,
            isConquistaCompleta = conquista.isConquistaCompleta,
            qtdeMatar = 0,
            tempoSobrevivencia = 0
        };

        if (conquista is ConquistaMatar)
        {
            c.qtdeMatar = (conquista as ConquistaMatar).qtdeMatar;
        }else if (conquista is ConquistaSobreviver)
        {
            c.tempoSobrevivencia = (conquista as ConquistaSobreviver).tempoSobrevivencia;
        }

        return c;
    }

    public virtual bool IsCompleto(int? valor)
    {
        return isConquistaCompleta;
    }

    public static ConquistaWrapper CarregarConquistas ()
    {
        if (File.Exists(PATH))
        {
            return JsonHandler.CarregarJson<ConquistaWrapper>(PATH);
        }else
        {
            var conquistasAssets = Resources.LoadAll<ConquistaObject>("Objects/Conquistas");
            List<Conquista> c = new List<Conquista>();

            foreach (var conquista in conquistasAssets)
            {
                c.Add(conquista);
            }

            ConquistaWrapper wrap = new ConquistaWrapper()
            {
                conquistas = c.ToArray()
            };

            return wrap;
        }
    }

    public static void RegistrarAlteracoes ()
    {
        ConquistaWrapper wrapper = new ConquistaWrapper();
        List<Conquista> c = new List<Conquista>();
        Camera.main.GetComponent<ConquistaController>().conquistas.ForEach(x => c.Add(x));
        wrapper.conquistas = c.ToArray();
        Conquista.SalvarConquistas(wrapper);
    }

    public static void SalvarConquistas (ConquistaWrapper conquistas)
    {
        JsonHandler.SalvarSobreescrever(conquistas, PATH);
    }

}
