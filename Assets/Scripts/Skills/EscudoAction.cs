﻿using UnityEngine;

[CreateAssetMenu(fileName = "EscudoAction", menuName = "Objetos/Actions/EscudoAction")]
public class EscudoAction : Action
{
    public int danosSuportados = 1;

    public override void Executar()
    {
        Debug.Log("PROTECTION");
        var player = GameObject.FindWithTag("Player").GetComponent<PersonagemController>();
        player.IsProtegido = true;
        var a = player.transform.Find("Escudo");
        a.gameObject.SetActive(true);
    }
}