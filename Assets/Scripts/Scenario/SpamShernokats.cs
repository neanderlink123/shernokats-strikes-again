﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Objetos/SpamShernokats")]
public class SpamShernokats : ScriptableObject {

    public List<GameObject> shernokats;

    public void LancarShernokat (Vector3 posicao)
    {
        var rand = Random.Range(0, shernokats.Count);
        Instantiate(shernokats[rand], posicao, Quaternion.identity);
    }

}
