﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Plataforma", menuName = "Objetos/Plataforma")]
public class GeraPlataforma : ScriptableObject, IGeradorPlataformas {

    public List<GameObject> plataformas;
    public GameObject ultimaPlataformaGround;
    public GameObject ultimaPlataformaTopo;
    public int distanciaMaxima = 20;

    private void OnEnable()
    {
        
    }

    private GameObject GerarPlataforma(GameObject plat)
    {
        if (Vector2.Distance(plat.transform.position, GameObject.FindWithTag("Player").transform.position) <= distanciaMaxima)
        {
            int rand = Random.Range(0, plataformas.Count);
            plat = GameObject.Instantiate(plataformas[rand], plat.transform.Find("Final").position, Quaternion.identity);
        }
        return plat;
    }

    public void Gerar()
    {
        ultimaPlataformaGround = GerarPlataforma(ultimaPlataformaGround);
        var platTopo = GerarPlataforma(ultimaPlataformaTopo);
        platTopo.transform.localScale = new Vector3(platTopo.transform.localScale.x, -1, platTopo.transform.localScale.z);
        ultimaPlataformaTopo = platTopo;
    }
}
