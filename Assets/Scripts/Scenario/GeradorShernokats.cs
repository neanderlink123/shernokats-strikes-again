﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class GeradorShernokats : MonoBehaviour
{
    public SpamShernokats shernokats;
    public Transform posiInicial, posiFinal;
    public float velocidade = 6f;

    private Rigidbody2D rb;
    private bool isEsperandoRespawn = false;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (transform.position.y >= posiInicial.position.y || transform.position.y <= posiFinal.position.y)
        {
            velocidade *= -1;
        }

        rb.velocity = new Vector2(0, velocidade);
        
        StartCoroutine(SpamarShernokats());
    }

    public IEnumerator SpamarShernokats ()
    {
        if (!isEsperandoRespawn)
        {
            isEsperandoRespawn = true;
            yield return new WaitForSeconds(3f);
            isEsperandoRespawn = false;
            shernokats.LancarShernokat(new Vector2(transform.position.x, transform.position.y));
        }
    }
}


