﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class GeradorCenario : MonoBehaviour
{
    public GeraPlataforma geraPlat;
    public GeraPlataformasVoadoras voadoras;
    public GeraBackground background;

    public float tempoAparecerBoss = 50f;

    public GameObject boss;
    public bool isBoss = false;
    private PontuacaoController p;

    bool trocouDificuldade = false;

    public void Start()
    {
        geraPlat.ultimaPlataformaGround = GameObject.Find("Ground_Inicial");
        geraPlat.ultimaPlataformaTopo = GameObject.Find("Upground_Inicial");
        voadoras.ultimaPlataforma = GameObject.Find("Plataforma_Inicial");
        background.ultimoFundo = GameObject.Find("Background_Ultimo");
        p = GameObject.FindWithTag("Player").GetComponent<PontuacaoController>();
        StartCoroutine(ContadorBoss());
    }

    public void LateUpdate()
    {
        if (!GameObject.FindWithTag("Player").GetComponent<PersonagemController>().IsMorto)
        {

            if (p.pontuacao > 25 && !trocouDificuldade)
            {
                var o = Resources.Load<GeraPlataforma>("Objects/Ground_Set_Medio");
                o.ultimaPlataformaGround = geraPlat.ultimaPlataformaGround;
                o.ultimaPlataformaTopo = geraPlat.ultimaPlataformaTopo;
                trocouDificuldade = true;
                geraPlat = o;
            }
            else
            {
                geraPlat.Gerar();
                background.Gerar();
                if (!isBoss)
                {
                    voadoras.Gerar();
                }
            }
        }

    }

    public IEnumerator ContadorBoss()
    {
        isBoss = false;
        bool msnBoss = false;
        for (int i = 0; i < tempoAparecerBoss; i++)
        {
            if (i >= (tempoAparecerBoss*0.8) && !msnBoss)
            {
                MessageController.MostrarMensagem("Prepare-se, o chefão está chegando!");
                msnBoss = true;
            }

            if (GameObject.FindWithTag("Player").GetComponent<PersonagemController>().IsMorto)
            {
                break;
            }

            yield return new WaitForSeconds(1f);
        }
        
        if (!GameObject.FindWithTag("Player").GetComponent<PersonagemController>().IsMorto)
        {
            isBoss = true;
            boss.SetActive(true);
        }
    }

}

