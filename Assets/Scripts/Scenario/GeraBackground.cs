﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "GeraBackground_", menuName = "")]
public class GeraBackground : ScriptableObject, IGeradorPlataformas
{
    public float distanciaMaxima = 20;
    public GameObject fundo;
    public GameObject ultimoFundo;

    public void Gerar()
    {
        ultimoFundo = GerarFundo(ultimoFundo);
    }

    private GameObject GerarFundo(GameObject ultimo)
    {
        if (Vector2.Distance(ultimo.transform.position, GameObject.FindWithTag("Player").transform.position) <= distanciaMaxima)
        {            
            ultimo = GameObject.Instantiate(fundo, ultimo.transform.Find("Final").position, Quaternion.identity);
        }
        return ultimo;
    }

}
