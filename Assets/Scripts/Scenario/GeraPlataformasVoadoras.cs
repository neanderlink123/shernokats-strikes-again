﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Plataformas_Voadoras", menuName = "Objetos/Plataformas Voadoras")]
public class GeraPlataformasVoadoras : ScriptableObject, IGeradorPlataformas
{
    public GameObject ultimaPlataforma;
    public List<GameObject> plataformas;
    public List<float> posicoesY;
     
    private void OnEnable()
    {
        
    }

    public void Gerar()
    {        
        ultimaPlataforma = GeraPlataforma(ultimaPlataforma);
    }

    private GameObject GeraPlataforma(GameObject last)
    {
        if (Vector2.Distance(last.transform.position, GameObject.FindWithTag("Player").transform.position) <= 20)
        {
            int randPosiY = Random.Range(0, posicoesY.Count);
            int randPlat = Random.Range(0, plataformas.Count);

            var posi = last.transform.Find("Final").position;
            posi.y = posicoesY[randPosiY];

            last = GameObject.Instantiate(plataformas[randPlat], posi, Quaternion.identity);
            
        }
        return last;
    }

}
