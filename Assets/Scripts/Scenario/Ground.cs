﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground : MonoBehaviour {

	// Use this for initialization
	void Start () {
        foreach (var s in transform.GetComponentsInChildren<Shernokat>())
        {
            s.GetComponent<Rigidbody2D>().gravityScale *= transform.localScale.y;
        }
        foreach (var item in GameObject.FindGameObjectsWithTag("Boss"))
        {
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), item.GetComponent<Collider2D>());
        }
    }
	
	// Update is called once per frame
	void FixedUpdate () {
		if (Vector2.Distance(GameObject.FindWithTag("Player").transform.position, transform.position) > 90)
        {
            Destroy(gameObject);
        }
	}

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
