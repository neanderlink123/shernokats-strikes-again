﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleShernokat : Shernokat
{
    public override void Start()
    {
        base.Start();
    }

    public override void DarDano(GameObject personagem)
    {
        if (personagem.CompareTag("Player"))
        {
            var p = personagem.GetComponent<PersonagemController>();
            p.RecebeuDano(dano);
            LancarMorte(transform.up, 10, 0);
        }
    }

    public override void OnCollisionEnter2D(Collision2D col)
    {
        DarDano(col.gameObject);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Bala"))
        {
            LancarMorte(transform.right, 10, 2);
            this.ControladorMissao().NotificarMissoes(TipoNotificacao.MatouShernokatIdle, 1);
            //Destroy(collision.gameObject);
        }
        else
        {
            DarDano(collision.gameObject);
        }
    }
}
