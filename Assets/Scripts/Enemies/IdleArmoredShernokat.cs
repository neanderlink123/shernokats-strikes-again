﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class IdleArmoredShernokat : Shernokat
{

    public override void Start()
    {
        base.Start();
    }
    public override void DarDano(GameObject personagem)
    {
        if (personagem.CompareTag("Player"))
        {
            var p = personagem.GetComponent<PersonagemController>();
            p.RecebeuDano(dano);
            LancarMorte(transform.up, 10, 0);
        }
    }

    public override void OnCollisionEnter2D(Collision2D col)
    {
        DarDano(col.collider.gameObject);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Bala"))
        {
            //Destroy(collision.gameObject);
        }
    }
}
