﻿using UnityEngine;
[RequireComponent(typeof(Rigidbody2D))]
public abstract class Shernokat : MonoBehaviour
{
    protected Rigidbody2D rb;    

    private AudioSource audioMorrendo;

    public virtual void Start()
    {
        audioMorrendo = GetComponent<AudioSource>();
        audioMorrendo.volume *= PlayerPrefs.GetFloat("Volume", 1);
        rb = GetComponent<Rigidbody2D>();

        foreach (var item in GameObject.FindGameObjectsWithTag("Boss"))
        {
            Physics2D.IgnoreCollision(item.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        }
    }

    public virtual void LancarMorte(Vector3 direcao, float forca, int qtdePontos)
    {
        GameObject.FindWithTag("Player").GetComponent<PontuacaoController>().AdicionarPontos(qtdePontos, transform.position);
        audioMorrendo.Play();

        if (rb)
        {
            transform.GetComponent<Collider2D>().enabled = false;
            rb.bodyType = RigidbodyType2D.Dynamic;
            rb.AddForce(direcao * forca, ForceMode2D.Impulse);
            rb.gravityScale = 1;
        }

        Conquista.RegistrarAlteracoes();
    }

    protected int dano = 1;
    public virtual void MovimentacaoPadrao() { }
    public abstract void OnCollisionEnter2D(Collision2D col);
    public abstract void DarDano(GameObject personagem);
}
