﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TheBoss : Shernokat
{
    public int vidas = 5;
    public List<GameObject> espinhas;

    public float velMove = 3;

    // Use this for initialization
    public override void Start()
    {
        base.Start();
        foreach (var item in GameObject.FindGameObjectsWithTag("Plataforma"))
        {
            if (item.name.StartsWith("Plataforma"))
            {
                Destroy(item);
            }
            else
            {
                Physics2D.IgnoreCollision(GetComponent<Collider2D>(), item.GetComponent<Collider2D>());
            }
        }
    }

    private void Update()
    {
        foreach (var item in FindObjectsOfType<Shernokat>())
        {
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), item.GetComponent<Collider2D>());
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (vidas > 0)
        {
            StartCoroutine(MostrarBossSuavemente());

            var v = new Vector3(Camera.main.transform.position.x + 5, transform.position.y, Camera.main.transform.position.z + 10);
            transform.position = v;

            transform.Translate(0, velMove * Time.deltaTime, 0);

            if (transform.position.y >= Camera.main.transform.Find("PosicaoInicial").position.y || transform.position.y <= Camera.main.transform.Find("PosicaoFinal").position.y)
            {
                velMove *= -1;
            }
        }
    }

    public IEnumerator MostrarBossSuavemente ()
    {
        for (float i = 0; i < 1; i+= 0.01f)
        {
            var c = Color.white;
            c.a = 0;
            var corAparece = Color.Lerp(c, Color.white, i);

            for (int j = 0; j < transform.childCount; j++)
            {
                var child = transform.GetChild(j);
                if (child.childCount > 0)
                {
                    var neto = child.GetChild(j);
                    for (int k = 0; k < child.childCount; k++)
                    {
                        neto.GetComponent<SpriteRenderer>().color = corAparece;
                    }
                }

                var srChild = child.GetComponent<SpriteRenderer>();
                if (srChild != null)
                {
                    child.GetComponent<SpriteRenderer>().color = corAparece;
                }
            }
            yield return new WaitForSeconds(0.01f);
        }        
    }

    public void ReceberDano()
    {
        vidas--;
        if (vidas > 0)
        {
            espinhas.Remove(espinhas[0]);
            espinhas[0].SetActive(true);
            if (espinhas.Count <= 1)
            {
                foreach (var item in GetComponentsInChildren<BarreiraController>())
                {
                    item.enabled = true;
                }
            }
            else if (espinhas.Count == 0)
            {
                gameObject.SetActive(false);
            }
        }
        else
        {
            GameObject.Find("PoolPlataformas").GetComponent<GeradorCenario>().isBoss = false;
            GameObject.Find("PoolPlataformas").GetComponent<GeradorCenario>().voadoras.ultimaPlataforma = gameObject;
            LancarMorte(Vector3.up, 15, 150);
        }
    }

    public override void OnCollisionEnter2D(Collision2D col)
    {

    }

    public override void DarDano(GameObject personagem)
    {

    }
}
