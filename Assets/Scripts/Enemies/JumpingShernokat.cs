﻿using System.Collections;
using UnityEngine;

public class JumpingShernokat : Shernokat
{
    private bool isExecutando = false;
    public float tempoEspera = 1f, forcaPulo = 3f;

    public int qtdeVidaRecuperar = 1;

    Animator anim;

    public override void Start()
    {
        base.Start();
        anim = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        if (rb.gravityScale < 0)
        {
            if (rb.velocity.y > 0)
            {
                anim.SetBool("IsFalling", true);
            }
            else
            {
                anim.SetBool("IsFalling", false);
            }
        }
        else
        {
            if (rb.velocity.y < 0)
            {
                anim.SetBool("IsFalling", true);
            }
            else
            {
                anim.SetBool("IsFalling", false);
            }
        }

        if (!isExecutando)
        {
            MovimentacaoPadrao();
        }
    }

    public override void MovimentacaoPadrao()
    {
        anim.SetBool("IsGrounded", false);
        rb.AddForce(rb.gravityScale > 0 ? transform.up * forcaPulo : transform.up * -forcaPulo, ForceMode2D.Impulse);
        StartCoroutine(EsperarParaPular());
    }

    public override void DarDano(GameObject personagem)
    {
        if (personagem.CompareTag("Player"))
        {
            PersonagemController p = personagem.GetComponent<PersonagemController>();
            p.RecebeuDano(dano);            
            LancarMorte(transform.right, 10, 0);
        }
    }

    public override void OnCollisionEnter2D(Collision2D col)
    {
        DarDano(col.collider.gameObject);
        if (col.collider.CompareTag("Plataforma"))
        {
            anim.SetBool("IsGrounded", true);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Bala"))
        {
            LancarMorte(transform.right, 10, 4);
            GameObject.Find("Player").GetComponent<PersonagemController>().AumentarVida(qtdeVidaRecuperar, transform.position);
            this.ControladorMissao().NotificarMissoes(TipoNotificacao.MatouShernokatPulador, 1);            
        }
    }

    private IEnumerator EsperarParaPular()
    {
        isExecutando = true;
        yield return new WaitForSeconds(tempoEspera);
        isExecutando = false;
    }

}
