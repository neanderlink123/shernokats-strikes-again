﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class EspinhaController : MonoBehaviour
{
    private void Update()
    {
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Collider2D>().CompareTag("Bala"))
        {
            GetComponentInParent<TheBoss>().ReceberDano();
            GameObject.FindWithTag("Player").GetComponent<PersonagemController>().AumentarVida(2, transform.position);
            gameObject.SetActive(false);
        }
    }
}
