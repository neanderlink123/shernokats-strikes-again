﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class BarreiraController : MonoBehaviour
{
    public int chances = 3;

    public void Start()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Collider2D>().CompareTag("Bala") && this.enabled)
        {
            chances--;
            if (chances < 0)
            {
                gameObject.SetActive(false);
            }
        }
    }

}
