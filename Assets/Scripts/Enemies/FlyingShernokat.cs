﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class FlyingShernokat : Shernokat
{
    public float velocidade = 6f;

    public override void Start()
    {
        if (GameObject.FindWithTag("Player").GetComponent<PersonagemController>().IsMorto)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            Destroy(gameObject, 5);
        }
        base.Start();
    }

    private void TrocarTransparencia (bool isTransparente)
    {
        var sr = GetComponent<SpriteRenderer>();
        
        if (isTransparente)
        {
            var cor = new Color(1, 1, 1, 0.4f);
            sr.color = cor;
        }else
        {
            var cor = Color.white;
            sr.color = cor;
        }
    }

    public virtual void OnBecameInvisible()
    {
        Destroy(gameObject, 2);
    }

    private void Update()
    {
        MovimentacaoPadrao();
    }

    public override void MovimentacaoPadrao()
    {
        rb.velocity = new Vector2(-velocidade, rb.velocity.y);
    }

    public override void DarDano(GameObject personagem)
    {
        PersonagemController p = personagem.GetComponent<PersonagemController>();
        p.RecebeuDano(1);
        LancarMorte(new Vector3(1, 1, 0).normalized, 8, 0);
    }

    public override void OnCollisionEnter2D(Collision2D col) { }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Bala"))
        {
            LancarMorte(transform.right, 10, 8);
            this.ControladorMissao().NotificarMissoes(TipoNotificacao.MatouShernokatVoador, 1);
        }
        if (collision.CompareTag("Player"))
        {
            DarDano(collision.gameObject);
        }

        
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Plataforma"))
        {
            TrocarTransparencia(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Plataforma"))
        {
            TrocarTransparencia(false);
        }
    }
}
