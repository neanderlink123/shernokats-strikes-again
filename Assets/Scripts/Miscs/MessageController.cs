﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class MessageController : MonoBehaviour
{
    public void Mostrar()
    {
        this.gameObject.SetActive(true);
    }

    public void Esconder ()
    {
        this.gameObject.SetActive(false);
    }

    public static void MostrarMensagem (string mensagem)
    {        
        var painel = GameObject.Find("Canvas").transform.Find("Painel_Mensagem");
        painel.GetComponentInChildren<Text>().text = mensagem;
        painel.GetComponent<MessageController>().Mostrar();
    }
}
