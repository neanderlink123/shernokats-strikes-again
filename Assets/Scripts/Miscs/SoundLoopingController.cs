﻿using UnityEngine;

public class SoundLoopingController : MonoBehaviour
{
    public float tempoLoop = 12.2f;
    public float tempoFinal = 23.2f;

    private void FixedUpdate()
    {
        var audio = GetComponent<AudioSource>();
        Debug.Log(audio.time);
        if (audio.time >= tempoFinal)
        {
            audio.time = tempoLoop;
            audio.Play();
        }
    }
}
