﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EventosMenu : MonoBehaviour
{
    public GameObject painelError;

    public void Start()
    {
        QualitySettings.SetQualityLevel(PlayerPrefs.GetInt("Qualidade", 1));
        try
        {
            SetarSom();
            AtualizarDnas();
        }
        catch (Exception) { }
    }

    private void SetarSom()
    {
        foreach (var audio in FindObjectsOfType<AudioSource>())
        {
            audio.volume *= PlayerPrefs.GetFloat("Volume", 1);
        }
    }

    public static void AtualizarDnas()
    {
        var totalDna = GameObject.Find("Canvas").transform.Find("PainelTotalDnas").GetComponentInChildren<Text>();
        totalDna.text = string.Format("x{0}", PlayerPrefs.GetFloat("TotalDna"));
    }

    public void IniciarJogo(GameObject containerInputs)
    {
        SceneManager.LoadScene("Prototipo", LoadSceneMode.Single);
    }

    public void ReiniciarCenaAtual()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void SairDoJogo()
    {
        Application.Quit();
    }

    public void MostrarErro(string mensagem)
    {
        painelError.GetComponentInChildren<Text>().text = mensagem;
        painelError.SetActive(true);
        StartCoroutine(EsperarFechar());
    }

    public void MostrarConquistas()
    {
        var wrapper = Conquista.CarregarConquistas();
        var painel = GameObject.Find("Canvas").transform.Find("Interface").transform.Find("PainelConquistas");
        var scrlConquistas = painel.Find("ScrollViewConquistas")
            .Find("Viewport")
            .Find("Content");

        foreach (var conq in wrapper.conquistas)
        {
            var prefabConquista = Resources.Load<GameObject>("Prefabs/UIs/Conquista").transform;
            prefabConquista.Find("TxtNome").GetComponent<Text>().text = conq.nome;
            prefabConquista.Find("TxtDescricao").GetComponent<Text>().text = conq.descricao;

            if (conq.qtdeMatar != 0)
            {
                prefabConquista.Find("TxtDescricao").GetComponent<Text>().text += string.Format("\nFaltam {0} Shernokats", conq.qtdeMatar);
            }

            prefabConquista.Find("TxtQtdeDna").GetComponent<Text>().text = "x" + conq.qtdeRecompensa;
            prefabConquista.Find("TglIsCompleto").GetComponent<Toggle>().isOn = conq.isConquistaCompleta;

            Instantiate(prefabConquista, scrlConquistas);
        }

        painel.gameObject.SetActive(true);
    }

    public void MostrarInventario()
    {
        var transicao = GameObject.Find("Canvas").transform.Find("Interface");
        transicao.GetComponent<Animator>().SetTrigger("Transicao");
    }

    public void MostrarLoja()
    {
        SceneManager.LoadScene("Menu_Loja");
    }

    public void FecharPainelConquistas()
    {
        var painel = GameObject.Find("Canvas").transform.Find("Interface").Find("PainelConquistas");
        var scrlConquistas = painel.transform.Find("ScrollViewConquistas")
            .Find("Viewport")
            .Find("Content");

        painel.gameObject.SetActive(false);

        for (int i = 0; i < scrlConquistas.childCount; i++)
        {
            var g = scrlConquistas.GetChild(i).gameObject;
            Destroy(g);
        }
    }

    public void VoltarMenuPrincipal()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Menu");
    }

    public void FecharPainelLoja()
    {
        var painel = GameObject.Find("Canvas").transform.Find("PainelLoja");
        painel.gameObject.SetActive(false);
    }

    public void AbrirCena(string nomeCena)
    {
        SceneManager.LoadScene(nomeCena);
    }

    public IEnumerator EsperarFechar()
    {
        yield return new WaitForSeconds(2f);
        FecharPainelErro();
    }

    public IEnumerator FecharSuave()
    {
        for (float i = 1; i >= 0; i -= 0.01f)
        {
            foreach (var item in painelError.GetComponentsInChildren<MaskableGraphic>())
            {
                Color c = item.color;
                c.a = i;
                item.color = c;
            }
            yield return new WaitForSeconds(0.01f);
        }
        painelError.SetActive(false);
        foreach (var item in painelError.GetComponentsInChildren<MaskableGraphic>())
        {
            Color c = item.color;
            c.a = 1;
            item.color = c;
        }
    }

    public void FecharPainelErro()
    {
        StartCoroutine(FecharSuave());
    }
}
