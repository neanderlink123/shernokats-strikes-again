﻿using System.Collections;
using System.Linq;
using UnityEngine;

public class ColecionavelController : MonoBehaviour
{
    public float tempoChegada = 1f, tempoTrocaCor = 1f;
    public Color corRecebendoColecionavel;

    public AudioSource audioColetando;

    public void Start()
    {
        audioColetando = GameObject.FindWithTag("Player").GetComponents<AudioSource>().Single(x => x.clip.name.Equals("Coletando"));
        StartCoroutine(IrAteOJogador(transform.position));
    }

    IEnumerator IrAteOJogador(Vector3 posicaoInicial)
    {
        for (float i = 0; i < tempoChegada; i += 0.01f)
        {
            var posiFinal = GameObject.FindWithTag("Player").transform.position;
            var posi = Vector3.Lerp(posicaoInicial, posiFinal, i / tempoChegada);

            var transparent = new Color(1, 1, 1, 0);
            var cor = Color.Lerp(Color.white, transparent, i / tempoChegada);

            transform.position = posi;
            GetComponent<SpriteRenderer>().color = cor;

            if (i / tempoChegada > 0.75f)
            {
                audioColetando.Play();
                var player = GameObject.FindWithTag("Player");
                player.GetComponent<PersonagemController>().StartCoroutine(EfeitoCorJogador(player.GetComponent<SpriteRenderer>(), tempoTrocaCor));
            }

            yield return new WaitForSeconds(0.01f);
        }
        Destroy(gameObject);
    }

    IEnumerator EfeitoCorJogador(SpriteRenderer spriteJogador, float tempoTrocaCor)
    {
        for (float i = 0; i <= tempoTrocaCor; i += 0.01f)
        {
            var cor = Color.Lerp(corRecebendoColecionavel, Color.white, i / tempoTrocaCor);
            spriteJogador.color = cor;
            yield return new WaitForSeconds(0.01f);
        }
    }

    public static void MostrarColecionavel(int qtdePontos, GameObject prefabColecionavel, Vector3 posicaoPadrao)
    {
        for (int i = 0; i < qtdePontos; i++)
        {
            var randIntX = Random.Range(-2, 2);
            var randIntY = Random.Range(-1, 1);
            var posiRand = posicaoPadrao;
            posiRand.x += randIntX;
            posiRand.y += randIntY;
            Instantiate(prefabColecionavel, posiRand, Quaternion.identity);
        }
    }

}
