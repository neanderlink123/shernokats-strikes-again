﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseController : MonoBehaviour
{
    public bool isPausado;
    public GameObject painelPause;

    // Use this for initialization
    void Start() { }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPausado)
            {
                Despausar();
            }
            else
            {
                Pausar();
            }
        }
    }

    public void Pausar()
    {
        MostrarPainelPause();
        isPausado = true;
        Time.timeScale = 0;
    }

    public void Despausar()
    {
        isPausado = false;
        EsconderPause();
        Time.timeScale = 1;
    }

    public void MostrarPainelPause ()
    {
        painelPause.SetActive(true);
    }

    public void EsconderPause ()
    {
        painelPause.SetActive(false);
    }

}
