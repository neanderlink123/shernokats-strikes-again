﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

public static class JsonHandler
{
    public static readonly string PATH_SAVE_DATA = ".\\SaveData\\";

    public static void SalvarSobreescrever<T>(T wrapper, string path) where T : IWrapper
    {
        string json = JsonUtility.ToJson(wrapper);
        var dir = path.Replace(path.Split('\\')[path.Split('\\').Length - 1], "");

        if (dir != "" && !Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }

        if (File.Exists(path))
        {
            File.Delete(path);
        }

        using (FileStream arquivo = new FileStream(path, FileMode.OpenOrCreate))
        {
            using (StreamWriter writer = new StreamWriter(arquivo))
            {
                writer.WriteLine(json);
            }
        }
    }

    public static T CarregarJson<T>(string path) where T : IWrapper
    {
        if (File.Exists(path))
        {
            using (var leitor = File.OpenText(path))
            {
                string strJson = leitor.ReadToEnd();
                var json = JsonUtility.FromJson<T>(strJson);
                return json;
            }
        }
        throw new ArgumentNullException("path", "Não foi encontrado um arquivo JSON no caminho fornecido.");
    }
}
