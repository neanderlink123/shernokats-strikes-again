﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    GameObject perso;
	// Use this for initialization
	void Start () {
        perso = GameObject.FindWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(perso.transform.position.x + 6, transform.position.y, transform.position.z);
    }
}
