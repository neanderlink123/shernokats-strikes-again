﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceController : MonoBehaviour
{
    PontuacaoController pontuacao;
    PersonagemController personagem;
    [Header("Personagem")]
    public GameObject painelVida;
    public GameObject painelEnergia;
    public GameObject imgVida, imgEnergia;

    [Header("Pontuação")]
    public Text txtPontos;
    public Text txtRecord;

    void Awake()
    {
        var g = GameObject.FindWithTag("Player");
        personagem = g.GetComponent<PersonagemController>();
        pontuacao = personagem.GetComponent<PontuacaoController>();
    }

    private void LateUpdate()
    {
        txtPontos.text = string.Format("DNAs coletados: {0:0}", pontuacao.pontuacao);
        txtRecord.text = string.Format("Seu record foi {0:0} DNAs", PlayerPrefs.GetFloat("PontuacaoMaxima", 0));
    }

    public static void LimparPainel(GameObject painel)
    {
        for (int i = 0; i < painel.transform.childCount; i++)
        {
            var g = painel.transform.GetChild(i).gameObject;
            Destroy(g);
        }
    }

    public static void SetarTextoHabilidade(string mensagem)
    {
        GameObject.Find("Canvas").transform.Find("TxtSkill").GetComponent<Text>().text = mensagem;
    }

    public void SetPainelVidas()
    {
        for (int i = 0; i < painelVida.transform.childCount; i++)
        {
            Destroy(painelVida.transform.GetChild(i).gameObject);
        }

        for (int i = 1; i <= personagem.VidaAtual; i++)
        {
            Instantiate(imgVida, painelVida.transform);
        }
    }

    public void SetPainelEnergias()
    {
        for (int i = 0; i < painelEnergia.transform.childCount; i++)
        {
            Destroy(painelEnergia.transform.GetChild(i).gameObject);
        }

        for (int i = 1; i <= personagem.QtdePulos; i++)
        {
            Instantiate(imgEnergia, painelEnergia.transform);
        }
    }
}