﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class Extensions
{
    public static ConquistaController ControladorMissao (this Component obj)
    {
        return Camera.main.GetComponent<ConquistaController>();
    }
}
