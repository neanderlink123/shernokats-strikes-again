﻿using Assets.Scripts.Repositories.ScriptableObjects;
using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PlayerScriptable : ScriptableObject
{
    public ItemScriptable arma;
    public ItemScriptable roupa;
    public ItemScriptable mochila;

    public SkillScriptable skill;

    public static explicit operator Player (PlayerScriptable serializable)
    {
        var player = new Player()
        {
            arma = (Item)serializable.arma,
            mochila = (Item)serializable.mochila,
            roupa = (Item)serializable.roupa,
            skill = (Skill)serializable?.skill ?? null
        };
        return player;
    }

    public static Player[] CastArrayToSerializable (PlayerScriptable[] scriptables)
    {
        var list = new List<Player>();

        foreach (var item in scriptables)
        {
            list.Add((Player)item);
        }

        return list.ToArray();
    }

}
