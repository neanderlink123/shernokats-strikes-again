﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Repositories.ScriptableObjects
{
    [CreateAssetMenu(fileName = "Item", menuName = "Objetos/Item...")]
    public class ItemScriptable : ScriptableObject
    {
        public string nome;
        public string descricao;
        public string pathSpriteIcone;
        public TipoItem tipoItem;
        [Tooltip("Só será computado o primeiro valor, com exceção de quando o tipo de item for Mochila, que o segundo será computado para as energias.")]
        public string[] valores;

        public bool isEquipado = false;
        public bool isComprado = false;

        public float valorCompra;

        public static explicit operator Item(ItemScriptable scriptable)
        {
            try
            {
                var item = new Item()
                {
                    nome = scriptable.nome,
                    descricao = scriptable.descricao,
                    pathSpriteIcone = scriptable.pathSpriteIcone,
                    isEquipado = scriptable.isEquipado,
                    tipoItem = scriptable.tipoItem,
                    valores = scriptable.valores,
                    isComprado = scriptable.isComprado,
                    valorCompra = scriptable.valorCompra
                };
                return item;
            }
            catch (Exception)
            {
                return null;
            }

        }

        public static explicit operator SkillScriptable(ItemScriptable scriptable)
        {
            var skill = ScriptableObject.CreateInstance<SkillScriptable>();
            skill.nome = scriptable.nome;
            skill.classeAction = scriptable.valores?[0];
            skill.pathIcone = scriptable.pathSpriteIcone;
            skill.valorDnas = (int)scriptable.valorCompra;
            skill.isEquipado = scriptable.isEquipado;
            return skill;
        }

        public static Item[] CastArrayToSerializable(ItemScriptable[] scriptables)
        {
            var list = new List<Item>();

            foreach (var item in scriptables)
            {
                list.Add((Item)item);
            }

            return list.ToArray();
        }

    }
}
