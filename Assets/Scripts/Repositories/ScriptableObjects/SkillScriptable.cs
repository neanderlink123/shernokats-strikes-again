﻿using Assets.Scripts.Repositories.ScriptableObjects;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Skill", menuName = "Objetos/Skill")]
public class SkillScriptable : ScriptableObject
{
    public string nome;
    public string pathIcone;

    public bool isEquipado;
    public string classeAction;
    public int valorDnas;

    public static explicit operator Skill(SkillScriptable serializable)
    {
        if (serializable)
        {
            var skill = new Skill()
            {
                nome = serializable.nome,
                pathIcone = serializable.pathIcone,
                isEquipado = serializable.isEquipado,
                classeAction = serializable.classeAction,
                valorDnas = serializable.valorDnas
            };
            return skill;
        }
        return null;
    }

    public static explicit operator ItemScriptable(SkillScriptable skill)
    {
        var item = CreateInstance<ItemScriptable>();

        item.nome = skill.nome;
        item.valores = new string[] { skill.classeAction };        
        item.pathSpriteIcone = skill.pathIcone;
        item.valorCompra = skill.valorDnas;
        item.tipoItem = TipoItem.Consumivel;
        item.isEquipado = skill.isEquipado;

        return item;
    }

    public static Skill[] CastArrayToSerializable(SkillScriptable[] scriptables)
    {
        var list = new List<Skill>();

        foreach (var item in scriptables)
        {
            list.Add((Skill)item);
        }

        return list.ToArray();
    }
}