﻿using Assets.Scripts.Repositories.Repos.Interfaces;
using RepositoryManager.Common;
using System;
using System.IO;
using System.Linq;
using System.Linq.Expressions;

namespace Assets.Scripts.Repositories.Repos
{
    public class PlayerRepository : IPlayerRepository
    {
        private ScriptableHelper _helper;
        public PlayerRepository()
        {
            if (!Directory.Exists(JsonHandler.PATH_SAVE_DATA))
            {
                Directory.CreateDirectory(JsonHandler.PATH_SAVE_DATA);
            }

            _helper = new ScriptableHelper(JsonHandler.PATH_SAVE_DATA + "playerprefs.json");
        }

        public PlayerScriptable[] GetAll()
        {
            var wrapper = _helper.LerJson<JsonWrapper<Player>>();

            if (wrapper.objetos.Length > 0)
            {
                var player = Player.CastArrayToScriptable(wrapper.objetos);
                return player;
            }
            return new PlayerScriptable[] { };
        }

        public PlayerScriptable[] GetMany(Expression<Func<PlayerScriptable, bool>> expressao)
        {
            var player = GetAll().AsQueryable().Where(expressao);
            return player.ToArray();
        }

        public PlayerScriptable GetSingle(Expression<Func<PlayerScriptable, bool>> expressao)
        {
            var player = GetAll().AsQueryable();
            return player.Single(expressao);
        }

        public void Salvar(PlayerScriptable[] players)
        {
            var wrapper = new JsonWrapper<Player>();
            wrapper.objetos = PlayerScriptable.CastArrayToSerializable(players);
            _helper.EscreverESalvarJson(wrapper);
        }
    }
}
