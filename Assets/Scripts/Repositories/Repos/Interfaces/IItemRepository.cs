﻿using Assets.Scripts.Repositories.ScriptableObjects;
using RepositoryManager.Common.Interfaces;

namespace Assets.Scripts.Repositories.Repos.Interfaces
{
    public interface IItemRepository : IBaseRepository<ItemScriptable>
    {
    }
}
