﻿using RepositoryManager.Common.Interfaces;

namespace Assets.Scripts.Repositories.Repos.Interfaces
{
    public interface IPlayerRepository : IBaseRepository<PlayerScriptable>
    {
    }
}
