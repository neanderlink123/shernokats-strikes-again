﻿using Assets.Scripts.Repositories.Repos.Interfaces;
using Assets.Scripts.Repositories.ScriptableObjects;
using RepositoryManager.Common;
using System;
using System.IO;
using System.Linq;
using System.Linq.Expressions;

namespace Assets.Scripts.Repositories.Repos
{
    public class ItemRepository : IItemRepository
    {
        private ScriptableHelper _helper;
        private readonly string _pathArquivo = JsonHandler.PATH_SAVE_DATA + "inventario.json";

        public ItemRepository()
        {
            if (!Directory.Exists(JsonHandler.PATH_SAVE_DATA))
            {
                Directory.CreateDirectory(JsonHandler.PATH_SAVE_DATA);
            }

            _helper = new ScriptableHelper(_pathArquivo);
        }

        public ItemScriptable[] GetAll()
        {
            var wrapper = _helper.LerJson<JsonWrapper<Item>>();

            wrapper = wrapper == null ? new JsonWrapper<Item>() { objetos = new Item[] { } } : wrapper;

            if (wrapper.objetos.Length > 0)
            {
                var itens = Item.CastArrayToScriptable(wrapper.objetos).OrderBy(x => x.nome);

                return itens.ToArray();
            }
            return new ItemScriptable[] { };
        }

        public ItemScriptable[] GetMany(Expression<Func<ItemScriptable, bool>> expressao)
        {
            var items = GetAll().AsQueryable().Where(expressao).OrderBy(x => x.nome);
            return items.ToArray();
        }

        public ItemScriptable GetSingle(Expression<Func<ItemScriptable, bool>> expressao)
        {
            var items = GetAll().AsQueryable();
            return items.Single(expressao);
        }

        public void Salvar(ItemScriptable[] items)
        {
            var wrapper = new JsonWrapper<Item>();
            wrapper.objetos = ItemScriptable.CastArrayToSerializable(items);
            _helper.EscreverESalvarJson<JsonWrapper<Item>>(wrapper);
        }
    }
}
