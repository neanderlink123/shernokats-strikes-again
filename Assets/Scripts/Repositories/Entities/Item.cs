﻿using Assets.Scripts.Repositories.ScriptableObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class Item
{
    public string nome;
    public string descricao;
    public string pathSpriteIcone;
    public TipoItem tipoItem;
    [Tooltip("Só será computado o primeiro valor, com exceção de quando o tipo de item for Mochila, que o segundo será computado para as energias.")]
    public string[] valores;

    public bool isEquipado = false;
    public bool isComprado = false;

    public float valorCompra;

    public static explicit operator ItemScriptable(Item item)
    {
        try
        {
            if (item.nome.Equals(""))
                throw new Exception();

            var scriptable = ScriptableObject.CreateInstance<ItemScriptable>();
            scriptable.nome = item.nome;
            scriptable.descricao = item.descricao;
            scriptable.pathSpriteIcone = item.pathSpriteIcone;
            scriptable.tipoItem = item.tipoItem;
            scriptable.isEquipado = item.isEquipado;
            scriptable.valores = item.valores;
            scriptable.isComprado = item.isComprado;
            scriptable.valorCompra = item.valorCompra;

            return scriptable;
        }
        catch (Exception)
        {
            return null;
        }
    }

    public static ItemScriptable[] CastArrayToScriptable(Item[] items)
    {
        var list = new List<ItemScriptable>();

        foreach (var item in items)
        {
            list.Add((ItemScriptable)item);
        }

        return list.ToArray();
    }

}

public enum TipoItem
{
    Roupa = 1,
    Arma = 2,
    Mochila = 3,
    Consumivel = 4
}
