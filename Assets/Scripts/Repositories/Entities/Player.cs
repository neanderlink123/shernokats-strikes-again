﻿using Assets.Scripts.Repositories.ScriptableObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class Player
{
    public Item arma;
    public Item roupa;
    public Item mochila;

    public Skill skill;

    public static explicit operator PlayerScriptable(Player player)
    {
        var scriptable = ScriptableObject.CreateInstance<PlayerScriptable>();
        scriptable.arma = (ItemScriptable)player.arma;
        scriptable.mochila = (ItemScriptable)player.mochila;
        scriptable.roupa = (ItemScriptable)player.roupa;
        scriptable.skill = (SkillScriptable)player.skill;

        return scriptable;
    }

    public static PlayerScriptable[] CastArrayToScriptable(Player[] scriptables)
    {
        var list = new List<PlayerScriptable>();

        foreach (var item in scriptables)
        {
            list.Add((PlayerScriptable)item);
        }

        return list.ToArray();
    }

}
