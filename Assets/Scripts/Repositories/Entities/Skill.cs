﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class Skill
{
    public string nome;
    public string pathIcone;

    public bool isEquipado;
    public string classeAction;
    public int valorDnas;

    public static explicit operator SkillScriptable(Skill skill)
    {
        var scriptable = ScriptableObject.CreateInstance<SkillScriptable>();
        scriptable.nome = skill.nome;
        scriptable.pathIcone = skill.pathIcone;
        scriptable.isEquipado = skill.isEquipado;
        scriptable.classeAction = skill.classeAction;
        scriptable.valorDnas = skill.valorDnas;

        return scriptable;
    }

    public static SkillScriptable[] CastArrayToScriptable(Skill[] objects)
    {
        var list = new List<SkillScriptable>();

        foreach (var item in objects)
        {
            list.Add((SkillScriptable)item);
        }

        return list.ToArray();
    }
}