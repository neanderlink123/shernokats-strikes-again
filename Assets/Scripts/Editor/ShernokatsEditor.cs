﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Editor
{
    public class ShernokatsEditor : EditorWindow
    {
        string textoQtde;
        int tipo;
        Sprite sprite;

        private void OnGUI()
        {
            GUILayout.Label("Reconfiguração de todos os Shernokats:", EditorStyles.boldLabel);
            tipo = EditorGUILayout.Popup("Filtrar por: ", tipo, new string[] { "Todos", "Idle Shernokats", "Flying Shernokats", "Jumping Shernokats", "Idle Armored Shernokats", "Chefão" });

            EditorGUILayout.Space();

            EdicaoVolume();

            EditorGUILayout.Space();

            GerenciaAnimator();

            EditorGUILayout.Space();

            GerenciaSprite();



            //if (GUILayout.Button("Trocar ground"))
            //{
            //    foreach (var item in Resources.FindObjectsOfTypeAll<Ground>().Where(x => x.name.Equals("Ground")))
            //    {
            //        item.GetComponent<SpriteRenderer>().sortingOrder = 5;
            //    }
            //}

        }

        [MenuItem("Shernokats/Configuração dos Shernokats")]
        public static void MostrarJanela()
        {
            GetWindow<ShernokatsEditor>("Shernokats");
        }

        private void GerenciaSprite()
        {
            sprite = (Sprite)EditorGUILayout.ObjectField("Novo sprite", sprite, typeof(Sprite), false);

            if (GUILayout.Button("Trocar Sprite", EditorStyles.miniButton))
            {
                TrocarSprite(tipo);
            }
        }

        private void GerenciaAnimator()
        {
            if (GUILayout.Button("Adicionar Animator", EditorStyles.miniButton))
            {
                AdicionarAnimator(tipo);
            }
            if (GUILayout.Button("Remover Animator", EditorStyles.miniButton))
            {
                RemoverAnimator(tipo);
            }
        }

        private void TrocarSprite(int tipo)
        {
            List<Shernokat> listaShernokats = GetListaShernokats(tipo);

            foreach (var item in listaShernokats)
            {
                item.GetComponent<SpriteRenderer>().sprite = sprite;
                item.GetComponent<SpriteRenderer>().flipX = false;
                item.GetComponent<SpriteRenderer>().color = Color.white;
                item.GetComponent<SpriteRenderer>().sortingOrder = -1;


                //var prefab = item.gameObject;
                DestroyImmediate(item.GetComponent<Collider2D>(), true);
                item.gameObject.AddComponent<CapsuleCollider2D>();

                item.transform.localScale = new Vector2(2.5f, 2.5f);
                item.transform.localPosition = new Vector2(item.transform.localPosition.x, 0.7f);
            }

            Debug.Log("Sprite trocado com sucesso!");
        }

        private void EdicaoVolume()
        {
            textoQtde = EditorGUILayout.TextField("Volume Audio de morte:", textoQtde, EditorStyles.numberField);
            if (GUILayout.Button("Trocar Volume", EditorStyles.miniButton))
            {
                Reconfigurar(tipo);
            }
        }

        private void Reconfigurar(int tipo)
        {
            try
            {
                var volume = float.Parse(textoQtde, System.Globalization.CultureInfo.InvariantCulture);
                TrocarVolume(volume, tipo);
                Debug.Log("Volume trocado com sucesso!");
            }
            catch (FormatException)
            {
                Debug.LogWarning("Texto digitado é inválido ou o campo está vazio.");
            }
            catch (ArgumentNullException)
            {
                Debug.LogWarning("A caixa de texto de volume não pode estar vazia.");
            }
            catch (Exception ex)
            {
                Debug.LogError("Houve um erro. Detalhes: " + ex.Message);
            }
        }

        private void TrocarVolume(float valor, int tipo)
        {
            List<Shernokat> listaShernokats = GetListaShernokats(tipo);

            foreach (var item in listaShernokats)
            {
                item.GetComponent<AudioSource>().volume = valor;
            }
        }

        private void AdicionarAnimator(int tipo)
        {
            List<Shernokat> listaShernokats = GetListaShernokats(tipo);

            foreach (var item in listaShernokats)
            {
                var anim = item.gameObject.AddComponent<Animator>();
                var animator = Resources.FindObjectsOfTypeAll<RuntimeAnimatorController>().SingleOrDefault(x => x.name.Equals("Enemy_Jumping"));
                anim.runtimeAnimatorController = animator;
            }
        }

        private void RemoverAnimator(int tipo)
        {
            List<Shernokat> listaShernokats = GetListaShernokats(tipo);

            foreach (var item in listaShernokats)
            {
                DestroyImmediate(item.GetComponent<Animator>(), true);
            }
        }

        private List<Shernokat> GetListaShernokats(int tipo)
        {
            var listaShernokats = new List<Shernokat>();
            switch (tipo)
            {
                case 0:
                    listaShernokats = Resources.FindObjectsOfTypeAll<Shernokat>().ToList();
                    break;
                case 1:
                    listaShernokats = Resources.FindObjectsOfTypeAll<Shernokat>().Where(x => x is IdleShernokat).ToList();
                    break;
                case 2:
                    listaShernokats = Resources.FindObjectsOfTypeAll<Shernokat>().Where(x => x is FlyingShernokat).ToList();
                    break;
                case 3:
                    listaShernokats = Resources.FindObjectsOfTypeAll<Shernokat>().Where(x => x is JumpingShernokat).ToList();
                    break;
                case 4:
                    listaShernokats = Resources.FindObjectsOfTypeAll<Shernokat>().Where(x => x is IdleArmoredShernokat).ToList();
                    break;
                case 5:
                    listaShernokats = Resources.FindObjectsOfTypeAll<Shernokat>().Where(x => x is TheBoss).ToList();
                    break;
                default:
                    listaShernokats = new List<Shernokat>();
                    break;
            }

            return listaShernokats;
        }
    }
}
