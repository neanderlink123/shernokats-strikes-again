﻿using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Editor
{
    public class CriarConquistasEditor : EditorWindow
    {
        private string nomeConquista, descricaoConquista;
        private int qtdeRecompensa, condicaoVitoria;

        private int tab;

        private void OnGUI()
        {
            RenderNome();

            GUILayout.Space(15);

            RenderTipo();

            RenderTabs();

            GUILayout.Space(15);
            GUILayout.Label("Atenção! Caso o nome de uma conquista seja igual ao nome de outra conquista existente,", EditorStyles.miniBoldLabel);
            GUILayout.Label("a nova conquista irá sobreescrevê-la!", EditorStyles.miniBoldLabel);
            if (RenderBotao())
            {
                AdicionarNovaConquista();
            }
        }

        private void RenderTabs()
        {
            switch (tab)
            {
                case 0:
                    qtdeRecompensa = EditorGUILayout.IntField("Valor da Recompensa: ", qtdeRecompensa, EditorStyles.numberField);
                    condicaoVitoria = EditorGUILayout.IntField("Inimigos a matar: ", condicaoVitoria, EditorStyles.numberField);
                    break;
                case 1:
                    qtdeRecompensa = EditorGUILayout.IntField("Valor da Recompensa: ", qtdeRecompensa, EditorStyles.numberField);
                    condicaoVitoria = EditorGUILayout.IntField("Tempo de sobrevivência: ", condicaoVitoria, EditorStyles.numberField);
                    break;
            }
        }

        private void AdicionarNovaConquista()
        {
            switch (tab)
            {
                case 0:
                    var mat = CriarConquistaMatar();
                    SalvarObjeto(mat, mat.nome);
                    break;
                case 1:
                    var sob = CriarConquistaSobreviver();
                    SalvarObjeto(sob, sob.nome);
                    break;
                default:
                    break;
            }
        }

        private static void SalvarObjeto(UnityEngine.Object objeto, string nomeArquivo)
        {
            AssetDatabase.CreateAsset(objeto, "Assets/Resources/Objects/Conquistas/Conquista_" + nomeArquivo + ".asset");
            AssetDatabase.Refresh(ImportAssetOptions.Default);
        }

        public ConquistaMatar CriarConquistaMatar()
        {
            var mat = CreateInstance<ConquistaMatar>();
            mat.nome = nomeConquista;
            mat.descricao = descricaoConquista;
            mat.qtdeMatar = condicaoVitoria;
            mat.qtdeRecompensa = qtdeRecompensa;

            return mat;
        }


        public ConquistaSobreviver CriarConquistaSobreviver()
        {
            var sob = CreateInstance<ConquistaSobreviver>();
            sob.nome = nomeConquista;
            sob.descricao = descricaoConquista;
            sob.tempoSobrevivencia = condicaoVitoria;
            sob.qtdeRecompensa = qtdeRecompensa;

            return sob;
        }

        private void RenderTipo()
        {
            GUILayout.Label("Escolha o tipo de conquista", EditorStyles.label);
            tab = GUILayout.Toolbar(tab, new string[] { "Matar", "Sobrevivência" });
        }

        private bool RenderBotao()
        {
            return GUILayout.Button("ADICIONAR NOVA CONQUISTA");
        }

        private void RenderNome()
        {
            GUILayout.Label("Adicionar uma nova conquista", EditorStyles.boldLabel);
            GUILayout.Label("Descreva a conquista", EditorStyles.label);

            nomeConquista = EditorGUILayout.TextField("Nome: ", nomeConquista, EditorStyles.textField);
            GUILayout.Label("Descrição:", EditorStyles.label);
            descricaoConquista = EditorGUILayout.TextArea(descricaoConquista, EditorStyles.textArea);
        }

        [MenuItem("Shernokats/Adicionar nova conquista")]
        public static void MostrarJanela()
        {
            GetWindow<CriarConquistasEditor>("Nova conquista");
        }
    }
}
