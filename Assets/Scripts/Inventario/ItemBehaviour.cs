﻿using Assets.Scripts.Repositories.Repos;
using Assets.Scripts.Repositories.ScriptableObjects;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ItemBehaviour : MonoBehaviour {

    public ItemScriptable item;
    public PlayerScriptable confs;
    public int qtdeItem = 0;   

    private PlayerRepository _playerRepository;    

    // Use this for initialization
    void Start () {
        _playerRepository = new PlayerRepository();
        confs = _playerRepository.GetSingle(x => x);
        Inicializar();
        GetComponent<Button>().onClick.AddListener(EquiparItem);
	}
	
    public void Inicializar ()
    {
        StartCoroutine(CarregarImagem(GetComponentsInChildren<Image>().First(x => x.name.Equals("ImgIcone")), item.pathSpriteIcone));
        GetComponentsInChildren<Text>().First(x => x.name.Equals("TxtTituloNome")).text = item.nome;
        GetComponentsInChildren<Text>().First(x => x.name.Equals("TxtDescricaoItem")).text = item.descricao;
        GetComponentsInChildren<Text>().First(x => x.name.Equals("TxtTipoItem")).text = item.tipoItem.ToString();
        GetComponentsInChildren<Text>().First(x => x.name.Equals("TxtQtdeItem")).text = $"x{qtdeItem}";
    }

    public static IEnumerator CarregarImagem (Image componenteImage, string pathImagem)
    {
        var request = Resources.LoadAsync<Sprite>(pathImagem);
        yield return request;
        while (!request.isDone)
        {
            Debug.Log("carregando... " + request.progress + " %");
            yield return null;
        }

        if (request.isDone)
        {
            componenteImage.sprite = (Sprite) request.asset;
        }
    }

    public void EquiparItem ()
    {
        item.isEquipado = !item.isEquipado;
        
        FindObjectOfType<InventarioController>().EditarItem(x => x.nome == item.nome, item);

        if (item.isEquipado)
        {
            GetComponent<Image>().color = Color.blue;
        }
        else
        {
            GetComponent<Image>().color = Color.gray;
        }
        TrocarEquipamentoNoMesmoSlot();
        FindObjectOfType<InventarioController>().SalvarInventario();
    }

    private void TrocarEquipamentoNoMesmoSlot()
    {
        var behaviour = FindObjectsOfType<ItemBehaviour>().FirstOrDefault(x => x.item.nome != item.nome && x.item.tipoItem == item.tipoItem && x.item.isEquipado);

        if (behaviour != null)
        {
            behaviour.item.isEquipado = false;
            behaviour.GetComponent<Image>().color = Color.gray;
        }
    }
}
