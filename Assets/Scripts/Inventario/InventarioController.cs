﻿using Assets.Scripts.Repositories.Repos;
using Assets.Scripts.Repositories.ScriptableObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class InventarioController : MonoBehaviour
{
    public List<ItemScriptable> inventario;
    public bool mostrarInventario;
    public ItemRepository itemRepository;

    public void Start()
    {
        itemRepository = new ItemRepository();
        inventario = itemRepository.GetAll().ToList();
        if (mostrarInventario)
            PopularInventario(x => x != null);
    }

    public void PopularInventario(Func<ItemScriptable, bool> expressao)
    {
        InterfaceController.LimparPainel(GameObject.Find("Canvas").transform.Find("PainelInventario").Find("ScrollItems").Find("Viewport").Find("Content").gameObject);

        var prefab = Resources.Load<GameObject>("Prefabs/UIs/PainelItem");
        var painelInventario = GameObject.Find("Canvas").transform.Find("PainelInventario").Find("ScrollItems").Find("Viewport").Find("Content");

        var grupo = inventario.GroupBy(x => new { x.nome }).Select(c => new { item = c.FirstOrDefault(x => x.nome != null), qtde = c.Count() }).ToList();

        grupo.ForEach((x) => { Debug.Log(x.item.nome + "-x" + x.qtde); });

        foreach (var i in grupo)
        {
            prefab.GetComponent<ItemBehaviour>().item = i.item;
            prefab.GetComponent<ItemBehaviour>().qtdeItem = i.qtde;

            if (i.item.isEquipado)
            {
                prefab.GetComponent<Image>().color = Color.blue;
            }
            else
            {
                prefab.GetComponent<Image>().color = Color.gray;
            }

            Instantiate(prefab, painelInventario);
        }

        SetarPlayerPrefs();
    }



    public void AdicionarItemNoInventario(ItemScriptable itemScriptable)
    {
        inventario.Add(itemScriptable);
    }

    public void SalvarInventario()
    {
        itemRepository.Salvar(inventario.ToArray());
        SetarPlayerPrefs();
    }

    public void EditarItem(Func<ItemScriptable, bool> predicate, ItemScriptable itemMudado)
    {
        var item = inventario.FirstOrDefault(predicate);
        if (item != null)
        {
            inventario.RemoveAt(inventario.IndexOf(item));
            inventario.Add(itemMudado);
        }
    }

    public static void SetarPlayerPrefs()
    {
        ItemRepository itemRep = new ItemRepository();
        PlayerRepository playerRepository = new PlayerRepository();

        var confs = ScriptableObject.CreateInstance<PlayerScriptable>();

        foreach (var item in itemRep.GetAll().Where(x => x.isEquipado))
        {
            if (item.isEquipado)
            {
                switch (item.tipoItem)
                {
                    case TipoItem.Roupa:
                        confs.roupa = item;
                        break;
                    case TipoItem.Arma:
                        confs.arma = item;
                        break;
                    case TipoItem.Mochila:
                        confs.mochila = item;
                        break;
                    case TipoItem.Consumivel:
                        confs.skill = (SkillScriptable)item;
                        break;
                    default: break;
                }
            }
        }

        playerRepository.Salvar(new PlayerScriptable[] { confs });
    }

    private void SetarBotaoSelecionado(Button botaoSelecionado)
    {
        var botoes = GameObject.Find("Canvas")
            .transform
            .Find("PainelInventario")
            .Find("BotoesFiltro")
            .GetComponentsInChildren<Button>();

        foreach (var botao in botoes)
        {
            botao.gameObject.GetComponent<Image>().color = Color.white;
            botao.GetComponentInChildren<Text>().color = Color.black;
        }

        botaoSelecionado.image.color = new Color(0.29f, 0.44f, 0.52f);
        botaoSelecionado.GetComponentInChildren<Text>().color = Color.white;
    }

    //Eventos
    public void FiltrarInventarioPorArma(Button botaoSelecionado)
    {
        InventarioController invController = Camera.main.GetComponent<InventarioController>();
        invController.PopularInventario(x => x.tipoItem == TipoItem.Arma);

        SetarBotaoSelecionado(botaoSelecionado);
    }

    public void FiltrarInventarioPorMochila(Button botaoSelecionado)
    {
        InventarioController invController = Camera.main.GetComponent<InventarioController>();
        invController.PopularInventario(x => x.tipoItem == TipoItem.Mochila);

        SetarBotaoSelecionado(botaoSelecionado);
    }

    public void FiltrarInventarioPorRoupa(Button botaoSelecionado)
    {
        InventarioController invController = Camera.main.GetComponent<InventarioController>();
        invController.PopularInventario(x => x.tipoItem == TipoItem.Roupa);

        SetarBotaoSelecionado(botaoSelecionado);
    }

    public void MostrarTodoInventario(Button botaoSelecionado)
    {
        InventarioController invController = Camera.main.GetComponent<InventarioController>();
        invController.PopularInventario(x => x != null);

        SetarBotaoSelecionado(botaoSelecionado);
    }

}
